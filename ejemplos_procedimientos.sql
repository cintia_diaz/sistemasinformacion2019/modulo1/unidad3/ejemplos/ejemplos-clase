﻿-- DROP DATABASE IF EXISTS programacion;
-- CREATE DATABASE programacion;

USE programacion;

/*
  Ejemplos de procedimientos almacenados
*/

  -- 26-08-2019

/*
  p1- Crear un procedimiento almacenado que muestre
  la fecha de hoy
*/

DELIMITER $$
CREATE OR REPLACE PROCEDURE p1()
  BEGIN
    SELECT NOW();
  END $$
DELIMITER ;
-- grabar=almacenar el procedimiento (seleccionado todoo)

CALL p1();
-- llamar=ejecutar el procedimiento


/*
  p2- Crear una variable de tipo datetime 
  y almacenar en esa variable la 
  fecha de hoy.
  Nos muestre la variable
*/

DELIMITER &&
CREATE OR REPLACE PROCEDURE p2()
  BEGIN
    -- declarar
    DECLARE v datetime;

    -- input (inicializar)
    SET v=NOW();

    -- output
    SELECT v;  
  END &&
DELIMITER ;

/*
  Creacion de una variable fuera de un procedimiento
SET @v=0;
SELECT @v;
-- @ es porque no estamos dentro de un procedimiento
*/

  CALL p2();

/*
  si dentro del procedimiento no quiero declarar la variable
  quiero que sea una variable inmediata, no tengo que declararla
  solo tengo que poner @v en set
*/

  -- 27-08-2019

  /*
  p3- SUMA 2 NUMEROS
      que recibe como argumentos dos numeros
      y me muestre la suma
  */

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE p3(numero1 int, numero2 int)
    BEGIN
      SELECT numero1+numero2;
    END $$
  DELIMITER ;

CALL p3(88,5);

-- p3 mejorado:
DELIMITER $$
CREATE OR REPLACE PROCEDURE p3(numero1 int, numero2 int)
  BEGIN
    DECLARE suma int DEFAULT 0;
    set suma=numero1+numero2;
    SELECT suma;
  END $$
DELIMITER ;

CALL p3(1,5);

CALL p3(88,2);

/*
  CREACION DE SNIPPETS:
  Tools -> Snippets Manager -> New y creamos
  Escribimos crearprocedimiento + ENTER
*/

 /*
   p4- RESTA 2 NUMEROS
      que recibe como argumentos dos numeros
      y me muestre la resta

*/
  DELIMITER $$
  CREATE OR REPLACE PROCEDURE p4(numero1 int, numero2 int)
    BEGIN
      DECLARE resultado int DEFAULT 0;
      SET resultado=numero1-numero2;
      SELECT resultado;      
    END $$
  DELIMITER ;
  
CALL p4(1,5);


/*
  p5- Crear un procedimiento que sume dos numeros y los almacene
  en una tabla llamada datos. 
  La tabla la debe crear en caso de que no exista
*/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE p5(numero1 int, numero2 int)
    BEGIN
      DECLARE resultado int DEFAULT 0;
      SET resultado=numero1+numero2;
      CREATE OR REPLACE TABLE misDatos (
          numero1 int,
          numero2 int,
          resultado int
        ); 
      INSERT INTO misDatos VALUES (numero1, numero2, resultado);
      SELECT * FROM misDatos d;
      
    END $$
  DELIMITER ;
  

CALL p5(5,4);
CALL p5(5,0);

-- p5 mejorado: solución Ramón

DELIMITER $$
CREATE OR REPLACE PROCEDURE p5_a(n1 int, n2 int)
  BEGIN
  -- declarar e inicializar las variables
  DECLARE resultado int DEFAULT 0;

  -- crear la tabla
  CREATE TABLE IF NOT EXISTS datos (
          id int AUTO_INCREMENT,
          d1 int DEFAULT 0,
          d2 int DEFAULT 0,
          suma int DEFAULT 0,
          PRIMARY KEY (id)
        );
-- replace es para crear una tabla temporal, con if not exists logramos que se guarde la tabla
-- y añadimos cada vez que llamemos al procedimiento
  -- realizar los calculos
  SET resultado=n1+n2;

  -- almacenar el resultado en la tabla
  INSERT INTO datos (id, d1, d2, suma) VALUES (DEFAULT, n1, n2, resultado);
  END $$

DELIMITER ;

CALL p5_a(1,6);
SELECT * FROM datos;


/*
  p6
  procedimiento que calcula la SUMA y el PRODUCTO de 2 numeros
  CALL p6(14,5)
  Crear una tabla llama sumaproducto(id, d1, d2, suma, producto)
  introducir los datos en la tabla y los resultado
*/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE p6(n1 int, n2 int)
    BEGIN
     DECLARE resultado1, resultado2 int DEFAULT 0;

     CREATE TABLE IF NOT EXISTS sumaproducto(
      id int AUTO_INCREMENT,
      d1 int DEFAULT 0,
      d2 int DEFAULT 0,
      suma int DEFAULT 0,
      producto int DEFAULT 0,
      PRIMARY KEY (id)
      );

    SET resultado1=n1+n2;
    SET resultado2=n1*n2;

    INSERT INTO sumaproducto (id, d1, d2, suma, producto)
      VALUES (DEFAULT, n1, n2, resultado1, resultado2);
      
    END $$
  DELIMITER ;
  
CALL p6(14,5);

SELECT * FROM sumaproducto;


  -- 02-09-2019
/*
  p7
  objetivo: elevar un numero a otro. Almacenar
  el resultado y los numeros en la tabla potencia
  argumentos: base y el exponente
  CALL p7(2,3) el resultado será 8
*/

    DELIMITER $$
    CREATE OR REPLACE PROCEDURE p7(base int, expon int)
      BEGIN
        DECLARE resultado int DEFAULT 0;

        CREATE TABLE IF NOT EXISTS potencia(
          id int AUTO_INCREMENT, 
          d1 int DEFAULT 0,
          d2 int DEFAULT 0,
          potencia int DEFAULT 0,
          PRIMARY KEY (id)
          );

       SET resultado=POW(base, expon);

       INSERT INTO potencia (id, d1, d2, potencia) -- campos
        VALUES (DEFAULT, base, expon, resultado);  -- variables

      END $$
    DELIMITER ;
    

  CALL p7(2,3);
  SELECT * FROM potencia;

  /*
    p8
    objetivo: realizar la raiz cuadrada de un numero
    Almacenar el numero y su raiz en la tabla raiz
  */

    DELIMITER $$
    CREATE OR REPLACE PROCEDURE p8(base int)
      BEGIN

        -- creando la variable
        DECLARE resultado float DEFAULT 0;


        -- realizo la potencia
        SET resultado=SQRT(base);

        CREATE TABLE IF NOT EXISTS raiz(
          id int AUTO_INCREMENT,
          b int,
          raiz float,
          PRIMARY KEY (id)
          );
        
        -- almaceno el dato en la tabla
        INSERT INTO raiz (b, raiz)      -- campos
           VALUES (base, resultado);    -- variables
        
      END $$
    DELIMITER ;
    
    CALL p8(64);
    CALL p8(39);
    SELECT * FROM raiz;


    /**
      Analizar el siguiente procedimiento y añadir
      lo necesario para que calcule la longitud del texto y lo
      almacene en la tabla en un campo llamado longitud
    **/

      DELIMITER $$
      DROP PROCEDURE IF EXISTS p9 $$
        -- drop + create es lo que estamos haciendo con el create or replace
      CREATE PROCEDURE p9(argumento varchar(50)) 
        BEGIN


          CREATE TABLE IF NOT EXISTS texto(
            id int AUTO_INCREMENT PRIMARY KEY,
            texto varchar(50)
            
            );

         INSERT INTO texto(texto) VALUE      -- nombre de la tabla, nombre del campo
           (argumento);                      -- nombre de la variable

      END $$
      DELIMITER ;

      -- modificacion
      DELIMITER $$
      DROP PROCEDURE IF EXISTS p9 $$
        
      CREATE PROCEDURE p9(argumento varchar(50)) 
        BEGIN

          DECLARE resultado int DEFAULT 0;
          
          SET resultado=CHAR_LENGTH(argumento);

          CREATE TABLE IF NOT EXISTS texto(
            id int AUTO_INCREMENT PRIMARY KEY,
            texto varchar(50),
            longitud int
            );

          INSERT INTO texto (texto, longitud)
          VALUES (argumento, resultado);
        END $$
      DELIMITER ;

      CALL p9('El perro de mi vecino ladra mucho');

      SELECT * FROM texto;
      

/**
  p10
  Procedimiento al cual le paso un numero y me indica si es mayor
  que 100
**/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE p10(numero int)
    BEGIN
      IF (numero>100) THEN
        SELECT "El numero es mayor que 100";
      END IF;
    END $$
  DELIMITER ;
  

CALL p10(123);
CALL p10(45);

/**
  p11
  Procedimiento al cual le pasas un numero y me indica
  si es mayor, igual o menor que 100
**/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE p11(numero int)
    BEGIN

      IF (numero>100) THEN
        SELECT "El numero es mayor que 100";
      ELSEIF (numero<100) THEN
        SELECT "El numero es menor que 100";
      ELSE 
        SELECT "El numero es 100";
      END IF;
      
    END $$
  DELIMITER ;

-- mejor programado aunque igual

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE p11modificado(numero int)
    BEGIN
      DECLARE resultado varchar(50) DEFAULT "igual a 100"; 

      IF (numero>100) THEN
        SET resultado="mayor que 100";
      ELSEIF (numero<100) THEN
        SET resultado="menor que 100";
      -- si no es ni menor ni mayor, mi suposicion es correcta luego ELSE sobra
      END IF;
      SELECT resultado;
      
    END $$
  DELIMITER ;

CALL p11modificado(123);
CALL p11modificado(23);
CALL p11modificado(100);


-- 03-09-19

  /**
  p12
  Crear un procedimiento que le pasas una nota y te debe devolver lo siguiente
  Suspenso: si la nota es menor que 5
  Suficiente: si la nota esta entre 5 y 7
  Notable: Si la nota esta entre 7 y 9
  Sobresaliente: si la nota es mayor que 9

  */

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE p12(nota float)
    BEGIN

    DECLARE resultado varchar(50) DEFAULT "Sobresaliente";

    IF (nota<5) THEN
      SET resultado="Suspenso";
    	
    ELSEIF (nota >=5 AND nota<7) THEN
      SET resultado="Suficiente";

    ELSEIF (nota>=7 AND nota<9) THEN
      SET resultado="Notable";   
    
    END IF;
    
    SELECT resultado;
      
    END $$
  DELIMITER ;
  
  CALL p12(6.8);
  CALL p12(8.2);
  CALL p12(9.21);
  CALL p12(7);
  CALL p12(5);
  CALL p12(3.0);
  CALL p12(9);

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE p12Ramon(n int)
    BEGIN
      DECLARE r varchar(50) DEFAULT "Suspenso";
  
      IF (nota >=5 AND nota<7) THEN
        SET r="Suficiente";
  
      ELSEIF (nota>=7 AND nota<9) THEN
        SET r="Notable";   
  
      ELSEIF (n>=9) THEN
        SET r="Sobresaliente";
      
      END IF;
      
    SELECT r;
      
    END $$
  DELIMITER ;
  
/**
  p13
  Realizar el mismo ejercicio anterior pero con CASE
**/
  -- modificado para CASE
  
    DELIMITER $$
    CREATE OR REPLACE PROCEDURE p13(n int)
      BEGIN
        DECLARE r varchar(50);
    
        CASE        
          WHEN (n >=5 AND n<7) THEN
            SET r="Suficiente";
      
          WHEN (n>=7 AND n<9) THEN
            SET r="Notable";   
      
          WHEN (n>=9) THEN
            SET r="Sobresaliente";
    
          ELSE 
            SET r="Suspenso";
  
        END CASE;     
        
      SELECT r;
        
      END $$
    DELIMITER ;

CALL p13(8);

  -- argumentos de Entrada/Salida
-- pasar argumento por valor o por referencia
/**
  p14
  Quiero calcular la suma de dos números. 
  La suma la debe almacenar en un argumento de salida llamada resultado
  CALL p14(n1 int, n2 int, OUT resultado int);

**/

    DELIMITER $$
    CREATE OR REPLACE PROCEDURE p14(n1 int, n2 int, OUT resultado int)
      BEGIN
        SET resultado=n1+n2;        
      END $$
    DELIMITER ;
    
    SET @suma=0;
    CALL p14(2, 5, @suma);
    SELECT @suma;

/**
      p15
      Procedimiento que recibe como argumentos:
      n1: numero (argumento de entrada)
      n2: numero (argumento de entrada)
      resultado: numero (argumento de salida o entrada/salida)
      calculo: texto  (argumento de entrada)
      Si calculo vale suma entonces resultado tendrá la suma de n1+n2
      Si calculo vale producto entonces resultado tendrá el producto de n1*n2
 **/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE p15(n1 int, n2 int, OUT resultado int, calculo varchar(10))
    BEGIN
      CASE 
        WHEN calculo="suma" THEN
          SET resultado=n1+n2;
        WHEN calculo="producto" THEN
          SET resultado=n1*n2; 
      END CASE;
      
    END $$
  DELIMITER ;
  


CALL p15(7,2,@r,"suma");
SELECT @r;

CALL p15(7,2,@r,"producto");
SELECT @r;

-- 04-09-19

-- Estructuras de Repetición. Sentencias de bucle

/**
  p16
  utilizando while
  con este procedimiento vamos a mostrar en pantalla numeros desde el 1 al 10
**/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE p16()
    BEGIN
    -- creamos una variable
      DECLARE contador int DEFAULT 1;
      
    -- creamos un bucle
      WHILE contador<=10 DO
        -- muestra la variable
        SELECT contador; 
        -- incrementamos la variable en 1
        SET contador=contador+1;
      
      END WHILE;     
      
    END $$
  DELIMITER ;
  

CALL p16();


/**
  p17
  utilizando while
  con este procedimiento vamos a crear una tabla TEMPORAL donde almacene del 1 al 10
  y a mostrar en pantalla la tabla
**/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE p17()
    BEGIN
    -- creamos una variable
    DECLARE contador int DEFAULT 1;

    -- Creamos una tabla temporal
    CREATE OR REPLACE TEMPORARY TABLE temporal(
      numeros int
      );
      
    -- creamos un bucle
    WHILE (contador<=10) DO
      INSERT INTO temporal VALUE (contador);
      SET contador = contador+1;  
    END WHILE; 
    SELECT * FROM temporal;    
      
    END $$
  DELIMITER ;
  

CALL p17();

/**
  p18
  Crear un procedimiento que le pasas un numero y te muestra desde el numero 1 hasta 
  el numero pasado. Utilizar una tabla temporal. Realizarlo con WHILE
  CALL p18(5); ==> 1,2,3,4,5
**/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE p18(n1 int)
    BEGIN

    -- Creamos variable
    DECLARE contador int DEFAULT 1;

    -- Creamos tabla temporal
    CREATE OR REPLACE TEMPORARY TABLE numeros(
      numero int
      );

    -- Realizamos bucle while, insertando valores en la tabla
    WHILE (contador<=n1) DO
      INSERT INTO numeros VALUE (contador); 
      SET contador=contador+1;    
    END WHILE;
    
    SELECT * FROM numeros;
      
    END $$
  DELIMITER ;

CALL p18(3);
-- 
  
DELIMITER $$
  CREATE OR REPLACE PROCEDURE p18b(n1 int)
    BEGIN

    -- Creamos variable
    DECLARE contador int DEFAULT 1;

    -- Creamos tabla temporal
    CREATE OR REPLACE TEMPORARY TABLE numerosb(
      numero int
      );

    -- Realizamos bucle REPEAT, insertando valores en la tabla
    
    REPEAT 
      INSERT INTO numerosb VALUE (contador);
      SET contador=contador+1;
    UNTIL (contador=n1+1)
    END REPEAT;
    
    
    SELECT * FROM numerosb;
      
    END $$
  DELIMITER ;

CALL p18b(6);

/**
  funciones
**/

/**
  f1
  le paso dos numeros y me devuelve la suma de ellos
**/
/*
  -- creamos snippet

DELIMITER &&
CREATE OR REPLACE FUNCTION f1()
  RETURNS int
BEGIN
 
  RETURN 1;
END &&
DELIMITER ;
*/

  DELIMITER &&
  CREATE OR REPLACE FUNCTION f1(n1 int, n2 int)
    RETURNS int
  BEGIN
    DECLARE resultado int DEFAULT 0;
   
    SET resultado=n1+n2;

    RETURN resultado;
  END &&

  DELIMITER ;

SELECT f1(5,6);


-- 17/09/2019

/*
  DISPARADORES
*/

CREATE OR REPLACE TABLE salas(
  id int AUTO_INCREMENT PRIMARY KEY,
  butacas int,
  fecha date,
  edad int DEFAULT 0
  );


CREATE OR REPLACE TABLE ventas(
  id int AUTO_INCREMENT PRIMARY KEY,
  sala int,
  numero int DEFAULT 0
  );

/** 
  SALASBI (Salas Before Insert)
  Controla la inserción de registros nuevos
  Disparador para que me calcule la edad de la sala en funcion de su fecha de alta
**/

DELIMITER |
 CREATE OR REPLACE TRIGGER salasBI
   BEFORE INSERT
     ON salas
     FOR EACH ROW
   BEGIN
     SET NEW.edad=TIMESTAMPDIFF(year,NEW.fecha, NOW());
   END |
DELIMITER ;


/* introduzco registro en la tabla para probar
   el disparador
*/

INSERT INTO salas (butacas, fecha)
  VALUES (50, '2000/1/1');

SELECT * FROM salas s;

/**
  SALASBU (Salas Before Update):
  Controla la actualización de registros
  Disparador para que me calcule la edad de la sala en función de su fecha de alta
**/

DELIMITER |
 CREATE OR REPLACE TRIGGER salasBU
   BEFORE UPDATE
     ON salas
     FOR EACH ROW
   BEGIN
     SET NEW.edad=TIMESTAMPDIFF(year, NEW.fecha, NOW());
   END |
DELIMITER ;

-- Comprobando el funcionamiento del disparador
UPDATE salas SET fecha='2001/1/1' WHERE id=1;

SELECT * FROM salas s;


-- Vamos a comprobar el funcionamiento de los DOS disparadores
INSERT INTO salas (butacas, fecha)
  VALUES (56,'1999/8/1'),(96,'2015/1/18');

SELECT * FROM salas s;

UPDATE salas s SET s.fecha='1990/1/1';

/**
  Necesito que la tabla SALAS disponga de tres campos nuevos.
  Estos campos serán dia, mes, año

  Quiero que esos tres campos AUTOMATICAMENTE tengan el día, 
  mes y año de la fecha

  Necesito un disparador para insertar y otro para actualizar
**/

ALTER TABLE salas 
  ADD COLUMN dia int,
  ADD COLUMN mes int,
  ADD COLUMN anio int;

DELIMITER |
 CREATE OR REPLACE TRIGGER salasBI
   BEFORE INSERT
     ON salas
     FOR EACH ROW
   BEGIN
     SET NEW.edad=TIMESTAMPDIFF(year, NEW.fecha, NOW());
     SET NEW.dia=TIMESTAMPDIFF(DAY, NEW.fecha, NOW());
     SET NEW.mes=TIMESTAMPDIFF(MONTH,NEW.fecha, NOW());
     SET NEW.anio=TIMESTAMPDIFF(YEAR,NEW.fecha, NOW());
   END |
DELIMITER ; 

INSERT INTO salas (butacas, fecha)
  VALUES (42, '2005/3/1');

SELECT * FROM salas s;

DELIMITER |
 CREATE OR REPLACE TRIGGER salasBU
   BEFORE UPDATE
     ON salas
     FOR EACH ROW
   BEGIN
     SET NEW.edad=TIMESTAMPDIFF(year, NEW.fecha, NOW());
     SET NEW.dia=TIMESTAMPDIFF(DAY, NEW.fecha, NOW());
     SET NEW.mes=TIMESTAMPDIFF(MONTH,NEW.fecha, NOW());
     SET NEW.anio=TIMESTAMPDIFF(YEAR,NEW.fecha, NOW());
   END |
DELIMITER ;

-- comprobacion del funcionamiento
INSERT INTO salas (butacas, fecha)
  VALUES (56,'2003/1/1'),
         (60,'2004/2/1');

INSERT INTO salas (butacas, fecha)
  VALUES (56,'2003/1/1');

UPDATE salas s SET s.edad=10;

/**
  Vamos a realizar un ejercicio que nos permita actualizar
  correctamente los datos en una nueva tabla

  Ventas1(id,precioUnitario,Unidades,precioFinal)

  Debe ser capaz de calcular el precioFinal con disparadores

  PrecioFinal=Unidades*precioUnitario

  Crear un disparador para inserción y para actualización
**/

CREATE OR REPLACE TABLE ventas1(
  id int AUTO_INCREMENT PRIMARY KEY,
  precioUnitario float,
  unidades int,
  precioFinal float
  );

DELIMITER |
 CREATE OR REPLACE TRIGGER ventas1BI 
   BEFORE INSERT 
     ON ventas1
     FOR EACH ROW
   BEGIN

     SET NEW.precioFinal=NEW.unidades*NEW.precioUnitario;

   END |
DELIMITER ;

DELIMITER |
 CREATE OR REPLACE TRIGGER ventas1BU
   BEFORE UPDATE
     ON ventas1
     FOR EACH ROW
   BEGIN

     SET NEW.precioFinal=NEW.unidades*NEW.precioUnitario;

   END |
DELIMITER ;

SELECT * FROM ventas1 v;

INSERT INTO ventas1 (precioUnitario, unidades)
  VALUES (12.3, 8),(5,4),(3.2,5);

UPDATE ventas1 v SET v.unidades=3
  WHERE id=2;