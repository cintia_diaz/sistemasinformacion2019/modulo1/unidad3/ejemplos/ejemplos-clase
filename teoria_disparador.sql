﻿CREATE TABLE productos(
	id int AUTO_INCREMENT PRIMARY KEY,
	nombre varchar(50),
	inicial char(1),
	precio float
);

INSERT INTO productos (nombre, precio) VALUES ('p1', 10);
INSERT INTO productos (nombre, precio) VALUES ('zapatos', 100);

SELECT LEFT(nombre,1) from productos;

-- ponemos iniciales en toda la tabla
UPDATE productos SET inicial=LEFT(nombre,1);

-- convertimos en disparador
 DELIMITER %%
  CREATE OR REPLACE TRIGGER productosBI
    BEFORE INSERT
      ON productos
      FOR EACH ROW
    BEGIN
      SET NEW.inicial=LEFT(NEW.nombre,1);
    END %%
 DELIMITER ;

INSERT INTO productos (nombre, precio) VALUES ('bolis', 100);


CREATE OR REPLACE TABLE pedidos(
  id int AUTO_INCREMENT PRIMARY KEY,
  idProducto int,
  nombreProducto varchar(50),
  unidades int,
  total float);

INSERT INTO productos (nombre, precio)
  VALUES ('pendrive',10);

INSERT INTO pedidos (idProducto, unidades)
  VALUES (1,150);

 DELIMITER %%
  CREATE OR REPLACE TRIGGER pedidosBI
    BEFORE INSERT
      ON pedidos
      FOR EACH ROW
    BEGIN
      SET NEW.nombreProducto=(SELECT nombre FROM productos WHERE id=NEW.idProducto);
      SET NEW.total=new.unidades*(SELECT precio FROM productos WHERE id=NEW.idProducto); -- AÑADIDO total
    END %%
 DELIMITER ;

INSERT INTO pedidos (idProducto, unidades)
  VALUES (2,150);
INSERT INTO pedidos (idProducto, unidades)
  VALUES (1,1500);
INSERT INTO pedidos (idProducto, unidades)
  VALUES (2,20);

-- consulta de actualizacion para todos los totales incluso de los anteriores

UPDATE pedidos pe JOIN productos pr ON pe.idProducto=pr.id 
  SET pe.total=pe.unidades*pr.precio;