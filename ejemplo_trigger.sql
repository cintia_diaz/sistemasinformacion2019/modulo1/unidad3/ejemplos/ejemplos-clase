﻿CREATE DATABASE IF NOT EXISTS ejemplotrigger;
USE ejemplotrigger;


CREATE OR REPLACE TABLE local(
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(50),
  plazas int,
  precio float
  );

CREATE OR REPLACE TABLE usuario(
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(50),
  inicial char(1)
  );

CREATE OR REPLACE TABLE usa(
  idLocal int,
  idUsuario int,
  total float,
  fecha date,
  dias int,
  PRIMARY KEY (idLocal, idUsuario)
  );


/*
  1- Quiero que cuando introduzca un usuario me calcule y almacene
     la inicial de su nombre
*/
  -- el nombre del disparador usuarioBI

     DELIMITER %%
      CREATE OR REPLACE TRIGGER usuarioBI
        BEFORE INSERT
          ON usuario
          FOR EACH ROW
        BEGIN
          SET NEW.inicial=LEFT(NEW.nombre,1);
        END %%
     DELIMITER ;
  
INSERT INTO usuario (nombre)
    VALUES ('Roberto');

SELECT * FROM usuario;

TRUNCATE usuario;

INSERT INTO usuario (nombre)
  VALUES ('roberto'), ('alicia'), ('juan');

/* Realiza parecido al disparador pero sobre toda la tabla (porque el set dentro del trigger es sólo sobre el registro
que estamos trabajando) */
UPDATE usuario SET inicial=LEFT(nombre,1);

/* modificamos la tabla usuarios añadiendo un campo nuevo: contador */

ALTER TABLE usuario
  ADD COLUMN contador int DEFAULT 0;

/**
  inserto un usuario en usa, me debe sumar a contador un 1 
**/

 DELIMITER |
  CREATE OR REPLACE TRIGGER usaAI
    AFTER INSERT
      ON usa
      FOR EACH ROW
    BEGIN

      UPDATE usuario
        SET contador=contador+1 
        WHERE id=NEW.idUsuario;

    /**
      update usuario 
        set contador=select count(*) from usa where id=new.usuario;
    **/

    END |
 DELIMITER ;

INSERT INTO usa (idLocal, idUsuario)
  VALUES (1,1),(1,2),(2,1);

SELECT * FROM usuario;
SELECT * FROM usa;

/*
  Consulta de actualizacion que realice lo que hace el disparador 
  anterior. 
  Me tiene que colocar en contador las veces que ése usuario sale
  en la tabla usa
*/

  -- numero de locales por usuario
  SELECT 
    idusuario, COUNT(*) 
  FROM usa 
  GROUP BY idUsuario;

  UPDATE usuario 
    SET contador=(SELECT COUNT(*) FROM usa u WHERE u.idUsuario=id);

  SELECT * FROM usuario u;

  /* c1 */
  SELECT * FROM usuario u JOIN usa u1 ON u.id=u1.idUsuario GROUP BY u1.idUsuario;

  /* optimizacion de c1 */
  SELECT u.idUsuario, COUNT(*) FROM usa u GROUP BY u.idUsuario;

/**
  UPDATE pedidos SET total=unidades*(SELECT precio FROM productos WHERE id=producto);
  UPDATE pedidos JOIN productos ON id=producto SET total=unidades*precio;
**/
  UPDATE usuario JOIN
    (SELECT u.idUsuario, COUNT(*) numero FROM usa u GROUP BY u.idUsuario) c1
  ON c1.idUsuario=id set contador=c1.numero;

  UPDATE usuario u
    SET contador=(
      SELECT COUNT(*) FROM usa u1 WHERE u1.idUsuario=id);

/**
  En usa calcular el total segun el precio del local y los numeros de días
  con un update y con un disparador cuando introducimos un dato nuevo
  en usa
**/


UPDATE usa u
  SET u.total=(SELECT precio*dias FROM local l WHERE l.id=u.idlocal);

/*
 DELIMITER |
  CREATE OR REPLACE TRIGGER localAU
    AFTER UPDATE
      ON local
      FOR EACH ROW
    BEGIN

      UPDATE usa u SET u.total=u.dias*(SELECT l.precio FROM local l WHERE l.id=u.idlocal);

    END |
 DELIMITER ;
*/

 DELIMITER |
  CREATE OR REPLACE TRIGGER usaBI
    BEFORE INSERT
      ON usa
      FOR EACH ROW
    BEGIN

      SET NEW.total=NEW.dias*(SELECT l.precio FROM local l WHERE l.id=NEW.idLocal);

    END |
 DELIMITER ;

TRUNCATE local;
INSERT INTO local (nombre, precio)
  VALUES ('local1',10),('local2',5),('local3',7);

SELECT * FROM local l;

/*
UPDATE usa u SET u.dias=5 WHERE u.idLocal=1;
UPDATE usa u SET u.dias=8 WHERE u.idLocal=2;
*/
TRUNCATE usa;
SELECT * FROM usa;
  
INSERT INTO usa (idLocal, idUsuario, dias)
  VALUES (1,1,5),(2,1,7),(1,3,80);

SELECT * FROM usa;

